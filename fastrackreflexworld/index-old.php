<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>AIRCLaunch</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
/* Style the video: 100% width and height to cover the entire window */
#myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%;
  min-height: 100%;
}

/* Add some content at the bottom of the video/page */

/* Style the button used to pause/play the video */


</style>
</head>

<body>

<video autoplay muted loop id="myVideo">
  <source src="vids/page.mp4" type="video/mp4">
</video>

<!-- Optional: some overlay text to describe the video -->



<div class="container-fluid">

    <div class="row mt-md-5">
     
      <div class="col-12 col-md-4 text-center  offset-md-4 mt-md-5">
		<div class="container">
        <div class="login text-center">
       
				<form id="login-form" method="post" role="form">
						<div id="login-message"></div>
				  <div class="form-group text-left">
                  
                  <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email Address :" name="email" id="email" required>
                  </div>
				 
                  <div class="form-group mt-4 text-center">
                  <input type="submit" class="btn btn-primary" value="submit"> 
                  </div>
				  
                </form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  $('#login').attr('disabled', true);
  $.post('register.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
          $('#login-message').text("You are registered successfully.");
          $('#login-message').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
          $("#login-form")[0].reset();
          $('#login').attr('disabled', true);
          return false;  
      }
      
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
</script>

</body>
</html>