<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Titan</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="assects/style.css">
<style>
/* Style the video: 100% width and height to cover the entire window */
#myVideo {
  position: absolute;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

/* Add some content at the bottom of the video/page */

/* Style the button used to pause/play the video */


</style>
</head>
<body>
<video  data-mask="10" playsinline="" autoplay="" muted="" loop="" accel-video="true" id="myVideo">
  <source src="vids/page.mp4" type="video/mp4">
</video>

<div id="msform_logo">
<img src="img/fastrack.png" alt="" class="img-fluid" style="width:300px;">
</div>

<!-- partial:index.partial.html -->
<!-- multistep form -->
<form id="msform" >


    
        
        <!--<label>Name</label>-->
<fieldset>
        <input type="text" id="data" name="name" placeholder="Enter Your Name" required />
        <input type="text" id="data" name="publisher" placeholder="Publication Name" required />
        <input type="email" id="data" name="email" placeholder="Enter Your Email" required />
        <input type="number" id="data" name="mobile" placeholder="Enter Your Mobile Number" />
        <input type="text" id="data" name="location" placeholder="Enter Your Location" />

        <input type="submit" name="next" class="next action-button" value="submit" />
</fieldset>
     
  
</form>

       

        
        

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<!-- jQuery easing plugin -->

<!-- partial -->
<script>
$(document).on('submit', '#msform', function()
{  $('#login').attr('disabled', true);
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
          location.href='webcast.php'; 
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
</script>

</body>
</html>
