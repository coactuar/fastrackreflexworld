-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 03:47 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fastrackreflexworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_empid` varchar(200) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `publisher_id` varchar(100) NOT NULL,
  `asked_at` varchar(200) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_empid`, `user_question`, `publisher_id`, `asked_at`, `speaker`, `answered`) VALUES
(3, 'Sangeetha Chengappa', 'sangeetha.chengappa@gmail.com', 'When were Reflex 2.0, Wav and Beat launched? Which years?', 'Hindu BusinessLine', '2021/03/05 12:43:17', 1, 0),
(4, 'Sangeetha Chengappa', 'sangeetha.chengappa@gmail.com', 'Is that lilac and grey that Suparna is wearing?\r\n', 'Hindu BusinessLine', '2021/03/05 12:58:53', 1, 0),
(5, 'Lakshmi Rajan', 'contactdesk@gmail.com', 'The Fastrack Hearables will be in your offline network stores? And what would be the pricing of the range.\r\nLakshmi Rajan / Tech Tamizha', 'Tech Tamizha', '2021/03/05 12:58:58', 1, 0),
(6, 'Mihir Chandra Srivastava', 'smihir308@gmail.com', 'What is your next plans.', 'Pioneer hindi', '2021/03/05 13:00:49', 1, 0),
(7, 'varun sharma', 'varun.sharma@jagrannewmedia.com', 'India is a very price sensitive market and Over the head headphone is a very competitive category, what\'s different is there for the youth in it? ', 'Dainik Jagran', '2021/03/05 13:00:51', 1, 0),
(8, 'Sangeetha Chengappa', 'sangeetha.chengappa@gmail.com', 'How does Fastrack intend to attract the more serious adventure seekers, - triathlon, deep sea diving enthusiasts , mountaineers etc who are currently buying Garmin products  ', 'Hindu BusinessLine', '2021/03/05 13:01:28', 1, 0),
(9, 'Avishek Rakshit', 'avishek.raks@gmail.com', 'Can you elaborate on the 2C Pay plan? Is it a mobile wallet? Have you tied up with any financial institution on this? \r\nAvishek Rakshit\r\nInformist (formerly called Cogencis Newswire)\r\n', 'Informist (formerly known as Cogencis Newswire)', '2021/03/05 13:03:10', 1, 0),
(10, 'Mihir Chandra Srivastava', 'smihir308@gmail.com', 'What is your next  plan \r\nMihir\r\nPioneer hindi \r\nLucknow', 'Pioneer hindi', '2021/03/05 13:03:56', 0, 0),
(11, 'JEEVANKUMAR A', 'jeevan.journo@gmail.com', 'How safe will be the data of customers?', 'Malayala manorama', '2021/03/05 13:04:01', 1, 0),
(12, 'Avishek Rakshit', 'avishek.raks@gmail.com', 'What is the expected growth in wearables in the next 2-3 years and how do you see it contributing to your consolidated rev and profit?\r\nAvishek Rakshit\r\nInformist', 'Informist (formerly known as Cogencis Newswire)', '2021/03/05 13:10:25', 0, 0),
(13, 'Sanjeev Kochhar', 'Kochhar.apr@gmail.com', 'Rates\r\n\r\nSanjeev Kochhar\r\nLifeInChandigarh.com', 'LifeInChandigarh.com', '2021/03/05 13:11:50', 0, 0),
(14, 'AVIK DAS', 'dasavik324@gmail.com', 'what is the contribution of wearables in the watches segment? And how much is it expected to be in 2-3 yrs   \r\nAvik Das\r\ntimes of india', 'times of indi', '2021/03/05 13:15:02', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reactions`
--

CREATE TABLE `tbl_reactions` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `reaction` varchar(50) NOT NULL,
  `reaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `publisher` varchar(100) NOT NULL,
  `location` varchar(200) DEFAULT NULL,
  `login_date` varchar(200) DEFAULT NULL,
  `logout_date` varchar(200) DEFAULT NULL,
  `joining_date` varchar(200) NOT NULL,
  `logout_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `mobile`, `publisher`, `location`, `login_date`, `logout_date`, `joining_date`, `logout_status`) VALUES
(1, 'Reynold', 'reynold@coact.co.in', 'n/a', 'Reynold', 'bangalore', '2021/03/05 10:36:15', '2021/03/05 11:30:17', '2021/03/03 16:55:36', 1),
(2, 'akshat', 'neeraj@coact.co.in', '674210', 's', 'Bng', '2021/03/17 15:29:34', '2021/03/17 15:39:04', '2021/03/03 17:56:59', 1),
(3, 'Akshat Jharia', 'akshatjharia@gmail.com', '7204420017', 'COACT', 'Bangalore', '2021/03/05 07:50:45', '2021/03/05 09:47:11', '2021/03/03 18:02:23', 1),
(4, 'Sujatha ', 'sujatga@coact.co.in', '9845563760', 'Business world ', 'Bangalore ', '2021/03/03 18:07:57', '2021/03/03 18:08:57', '2021/03/03 18:07:57', 0),
(5, 'Akshat', 'akshat@coact.com', '7204420017', 'Coact', 'Bng', '2021/03/03 18:21:30', '2021/03/03 18:22:30', '2021/03/03 18:21:30', 0),
(6, 'Sujatha ', 'sujatha@coact.co.in', '9845563760', 'Business world ', 'Bangalore ', '2021/03/05 10:35:37', '2021/03/05 12:40:28', '2021/03/04 09:39:19', 1),
(7, 'Nishanth', 'nishanth@coact.co.in', '8747973536', 'COACT', 'Bangalore', '2021/03/05 09:46:37', '2021/03/05 13:23:51', '2021/03/04 10:04:01', 1),
(8, 'Syed Aakif', 'content@hammerheadexperiences.com', '8778281594', 'HH', 'bangalore', '2021/03/04 11:09:47', '2021/03/04 13:44:56', '2021/03/04 11:09:47', 0),
(9, 'Siddhu Raju', 'siddhur@titan.co.in', '09880302478', 'Times', 'Bangalore', '2021/03/05 12:29:15', '2021/03/05 13:17:48', '2021/03/04 11:30:15', 1),
(10, 'alankar baranwal', 'alankarbaranwal@gmail.com', '07505117007', 'Swatantra bharat', 'Lucknow', '2021/03/05 12:27:48', '2021/03/05 12:55:50', '2021/03/04 11:45:25', 1),
(11, 'Adfactors PR', 'vineesh.swarup@adfactorspr.com', '8054077299', 'Adfactors PR', 'Chandigarh', '2021/03/04 15:42:25', '2021/03/04 15:48:57', '2021/03/04 12:09:15', 1),
(12, 'Jayesh', 'Jayeshlakhani1@gmail.com', '9900975013', 'HHE', 'BLR', '2021/03/04 12:25:23', '2021/03/04 13:48:25', '2021/03/04 12:25:23', 0),
(13, 'pooja', 'poojasharma@titan.co.in', '9', 'NA', 'Bangalore', '2021/03/05 12:22:00', '2021/03/05 13:16:31', '2021/03/04 12:27:19', 1),
(14, 'Stuti Rishi', 'stuti.rishi@adfactorspr.com', '9810896935', 'ADF', 'Bangalore', '2021/03/05 11:43:50', '2021/03/05 13:16:25', '2021/03/04 12:27:45', 1),
(15, 'Kirti Verma', 'kirtiverma@titan.co', '9820490522', 'Titan', 'Bangalore', '2021/03/04 12:27:50', '2021/03/04 13:39:34', '2021/03/04 12:27:50', 0),
(16, 'Pooja', 'pooja.sharma.v@gmail.com', '09773309256', 'Na', 'Na', '2021/03/04 12:28:38', '2021/03/04 13:43:39', '2021/03/04 12:28:38', 0),
(17, 'neil', 'neil.luis@adfactorspr.com', '9987100460', 'Adfactors', 'Mumbai', '2021/03/05 12:19:20', '2021/03/05 13:18:21', '2021/03/04 12:29:01', 1),
(18, 'Momta', 'momita@titan.co.in', '9980557431', 'TItan', 'blr', '2021/03/05 12:22:04', '2021/03/05 14:38:17', '2021/03/04 12:29:24', 1),
(19, 'Vandana M', 'vandana.m@adfactorspr.com', '9999479947', 'Adfactors PR', 'Bangalore', '2021/03/05 12:13:46', '2021/03/05 13:16:34', '2021/03/04 12:29:51', 1),
(20, 'ki', 'test@live.com', '9820490522', 'Titan', 'bangalore', '2021/03/04 12:32:40', '2021/03/04 14:02:11', '2021/03/04 12:32:40', 0),
(21, 'Kirti Verma', 'kirti@gmail.com', '9820490522', 'Titan', 'Bangalore', '2021/03/04 12:33:33', '2021/03/04 14:02:04', '2021/03/04 12:33:33', 0),
(22, 'Ramakrishna', 'ramakrishnav@titan.co.in', '9901904650', 'Titan', 'Bengaluru', '2021/03/04 12:38:29', '2021/03/04 13:32:02', '2021/03/04 12:38:29', 0),
(23, 'Karthik', 'cmskarthik@titan.co.in', '9008226281', 'corp', 'Corp', '2021/03/04 12:54:46', '2021/03/04 12:55:47', '2021/03/04 12:54:46', 0),
(24, 'Ramesh Kumar', 'helpdesksdm@titan.co.in', '9940172327', 'Ramesh Kumar', 'Bangalore', '2021/03/04 12:56:53', '2021/03/04 12:57:53', '2021/03/04 12:56:53', 0),
(25, 'Sameer Zaine', 'cs1@hammerheadexperiences.com', '9886403693', 'Hammer Head ', 'Bangalore ', '2021/03/05 13:09:40', '2021/03/05 13:33:41', '2021/03/04 12:57:18', 1),
(26, 'Khaleel', 'accounts@hammerexperiences.com', 'n/a', 'Hammer Head Experiences Pvt Ltd', 'bangalore', '2021/03/04 13:13:09', '2021/03/04 13:16:40', '2021/03/04 13:13:09', 0),
(27, 'Anuja Salvi', 'anujasalvi@titan.co.in', '9664676989', 'Titan', 'Bangalore', '2021/03/05 12:24:03', '2021/03/05 13:10:40', '2021/03/04 13:15:02', 1),
(28, 'Aakif', 'hdhdhd@hdhd.com', '65665', 'G', 'Gdhhd', '2021/03/04 13:23:25', '2021/03/04 13:24:25', '2021/03/04 13:23:25', 0),
(29, 'AJAY  MAURYA', 'ajaym@titan.co.in', '9686663694', 'POP', 'Bangalore', '2021/03/04 13:27:49', '2021/03/04 14:47:42', '2021/03/04 13:27:49', 0),
(30, 'Ritika Gosain', 'ritika.gosain@adfactors.pr.com', '8872122242', 'Adfactors PR', 'chandigarh', '2021/03/04 15:39:58', '2021/03/04 15:40:58', '2021/03/04 15:39:58', 0),
(31, 'Ashok', 'ashokpa@cybermedia.co.in', '9910022488', 'Cybermedia', 'Gurugram', '2021/03/04 16:34:46', '2021/03/04 16:35:46', '2021/03/04 16:34:46', 0),
(32, 'Srinivas', 'kumar.adfactorshyd@gmail.com', '9392632985', 'Mana Telangana', 'hyderabad', '2021/03/04 17:04:52', '2021/03/04 17:05:52', '2021/03/04 17:04:52', 0),
(33, 'Priya Singh', 'Priya.singh@nw18.com', '08860252761', 'Tech2 (Fastrack)', 'Bulandshahr', '2021/03/05 12:35:04', '2021/03/05 13:16:35', '2021/03/05 09:34:51', 1),
(34, 'Roopa Prabhu', 'roopaprabhu@titan.co.in', 'n/a', 'Titan', 'n/a', '2021/03/05 12:43:17', '2021/03/05 13:10:18', '2021/03/05 10:19:40', 1),
(35, 'Mihir Chandra Srivastava', 'smihir308@gmail.com', '9336750839', 'Pioneer hindi', 'Lucknow', '2021/03/05 13:02:00', '2021/03/05 13:07:33', '2021/03/05 10:26:48', 1),
(36, 'Syed Aakif', 'sydaakif@gmail.com', '8778281594', 'HHE', 'Bangalore', '2021/03/05 10:49:29', '2021/03/05 10:55:58', '2021/03/05 10:49:29', 0),
(37, 'Akshay', 'akshay.kumar@adfactorspr.com', '7838350565', 'AFPR', 'Delhi', '2021/03/05 11:40:04', '2021/03/05 13:15:58', '2021/03/05 11:40:04', 0),
(38, 'Derek', 'derek@titan.co.in', '9444985142', 'SBI', 'blre', '2021/03/05 11:46:02', '2021/03/05 13:15:37', '2021/03/05 11:46:02', 0),
(39, 'Chetali Joshi', 'chetali.joshi@adfcatorspr.com', '8427302331', 'Adfactors PR', 'Delhi', '2021/03/05 11:47:31', '2021/03/05 11:48:31', '2021/03/05 11:47:31', 0),
(40, 'Lavang Khare', 'lavang.khare@adfactorspr.com', '09810077122', 'Adfactors PR', 'ghaziabad', '2021/03/05 12:16:28', '2021/03/05 13:23:29', '2021/03/05 11:52:06', 1),
(41, 'Erick Massey', 'masseyerick1@gmail.com', '9999357901', 'Business Standard', 'Delhi', '2021/03/05 12:39:02', '2021/03/05 13:18:02', '2021/03/05 11:55:04', 1),
(42, 'RD', 'duttaromen01@gmail.com', 'n/a', 'The Echo of India', 'Kolkata', '2021/03/05 12:05:55', '2021/03/05 13:19:46', '2021/03/05 12:05:55', 0),
(43, 'SHEKAR B', 'shekas14@gmail.com', '09538757922', 'Adfactors PR ', 'Bangalore', '2021/03/05 12:08:08', '2021/03/05 13:15:09', '2021/03/05 12:08:08', 0),
(44, 'TB venugopal', 'tb.venugopal@adfactorspr.com', '19847041616', 'adfactorsPr', 'Kochi', '2021/03/05 12:12:10', '2021/03/05 13:15:41', '2021/03/05 12:12:10', 0),
(45, 'Sangeetha Chengappa', 'sangeetha.chengappa@gmail.com', '09886733807', 'Hindu BusinessLine', 'Bengaluru', '2021/03/05 12:26:58', '2021/03/05 16:56:33', '2021/03/05 12:13:23', 1),
(46, 'Sanjeev Kochhar', 'Kochhar.apr@gmail.com', '09815617676', 'LifeInChandigarh.com', 'Chandigarh', '2021/03/05 13:16:50', '2021/03/05 13:17:50', '2021/03/05 12:13:37', 1),
(47, 'Sujith Gopinath', 'sujithgopinath@gmail.com', '+919820642520', 'T3 India', 'Mumbai', '2021/03/05 12:16:44', '2021/03/05 13:16:12', '2021/03/05 12:13:39', 1),
(48, 'Ahuja', 'kbahuja29@gmail.com', '9999227852', 'Dainik Savera', 'Chandigarh', '2021/03/05 12:14:37', '2021/03/05 12:55:47', '2021/03/05 12:14:37', 0),
(49, 'varun sharma', 'varun.sharma@jagrannewmedia.com', '9910879394', 'Dainik Jagran', 'Delhi', '2021/03/05 12:59:13', '2021/03/05 13:13:43', '2021/03/05 12:14:46', 1),
(50, 'Chetali Johsi', 'chetali.joshi@adfactorspr.com', '8427302331', 'Adfactors PR ', 'Delhi', '2021/03/05 12:14:51', '2021/03/05 13:36:11', '2021/03/05 12:14:51', 0),
(51, 'Ashraf', 'ashrafthaivalapu@gmail.com', '9995712102', 'Chandrika Daily', 'Kochi', '2021/03/05 12:16:32', '2021/03/05 12:33:43', '2021/03/05 12:16:32', 0),
(52, 'Avishek Rakshit', 'avishek.raks@gmail.com', '9831477912', 'Informist (formerly known as Cogencis Newswire)', 'Kolkata', '2021/03/05 12:33:27', '2021/03/05 13:16:57', '2021/03/05 12:17:10', 1),
(53, 'Jeevan Chandy', 'jeevan.chandy@adfactorspr.com', '9447302033', 'Adfactors ', 'Kochi', '2021/03/05 12:17:24', '2021/03/05 13:15:39', '2021/03/05 12:17:24', 0),
(54, 'Kirti Verma', 'kirtiverma@titan.co.in', '9820490522', 'Titan', 'Bangalore', '2021/03/05 12:17:27', '2021/03/05 13:15:24', '2021/03/05 12:17:27', 0),
(55, 'Anne chacko', 'anne@titan.co.in', '9632576232', 'Titan', 'Bangalore', '2021/03/05 12:22:54', '2021/03/05 14:09:46', '2021/03/05 12:19:18', 1),
(56, 'jayesh Lakhani', 'gm@hammerheadexperiences.com', '9900975013', 'HHE', 'Bangalore', '2021/03/05 12:22:45', '2021/03/05 13:19:48', '2021/03/05 12:22:45', 0),
(57, 'Yuthika Bhargava', 'yuthikabhargava@gmail.com', '08860633140', 'The hindu', 'Noida', '2021/03/05 12:25:14', '2021/03/05 13:17:15', '2021/03/05 12:25:14', 0),
(58, 'Sreejesh Suresh', 'sreejesh@techgyo.com', '08884000424', 'TechGYO', 'Bangalore', '2021/03/05 12:26:18', '2021/03/05 13:15:20', '2021/03/05 12:26:18', 0),
(59, 'Parul Bhargava', 'parul.bhargava@adfactorspr.com', '9044010101', 'Adfactors PR', 'Lucknow', '2021/03/05 12:26:30', '2021/03/05 13:09:31', '2021/03/05 12:26:30', 0),
(60, 'Vinay Kumar Tewari ', 'vkttewari@gmail.com', '6394180664', 'Aaj', 'Gomtinagar ', '2021/03/05 12:28:04', '2021/03/05 12:28:43', '2021/03/05 12:28:04', 0),
(61, 'Smita Balram', 'smitabalram@gmail.com', '09945528001', 'The Economic Times', 'Bangalore East', '2021/03/05 12:29:15', '2021/03/05 13:15:45', '2021/03/05 12:29:15', 0),
(62, 'Tushar', 'tushar.patowary70@gmail.com', '8240062389', 'Lipi', 'Kolkata', '2021/03/05 12:30:01', '2021/03/05 13:15:02', '2021/03/05 12:30:01', 0),
(63, 'Vijay', 'vijayjoshi@prajavani.co.in', '9845974719', 'Prajavani', 'Bengaluru', '2021/03/05 12:30:30', '2021/03/05 12:51:29', '2021/03/05 12:30:30', 0),
(64, 'Roshan R', 'roshan36@gmail.com', '9847475307', 'Mathrubhumi ', 'Kochi', '2021/03/05 12:34:59', '2021/03/05 13:01:26', '2021/03/05 12:31:32', 1),
(65, 'Derek', 'derek.d.kuttikkat@gmail.com', '09444985142', 'Sbi', 'Cochin', '2021/03/05 12:31:33', '2021/03/05 12:37:22', '2021/03/05 12:31:33', 0),
(66, 'suneera tandon', 'suneera.t@htlive.com', '9810445443', ' MINT', 'New Delhi', '2021/03/05 12:31:35', '2021/03/05 13:10:16', '2021/03/05 12:31:35', 0),
(67, 'Ritika Gosain', 'ritika.gosain@adfactorspr.com', '8872122242', 'Adfactors PR ', 'Chandigarh', '2021/03/05 12:32:28', '2021/03/05 12:38:53', '2021/03/05 12:32:28', 0),
(68, 'Somprabh Kumar Singh', 'somprabh@titan.co.in', '09972028432', 'Titan', 'Bengaluru', '2021/03/05 12:33:05', '2021/03/05 13:15:53', '2021/03/05 12:33:05', 0),
(69, 'Deep singh', 'deep25singhlko@gmail.com', '9450002800', 'Nav bharat times ', 'Lucknow', '2021/03/05 12:33:13', '2021/03/05 12:44:14', '2021/03/05 12:33:13', 0),
(70, 'Loknath', 'loknath@gmil.com', 'n/a', 'KP', 'Bangalore', '2021/03/05 12:34:08', '2021/03/05 12:45:45', '2021/03/05 12:34:08', 0),
(71, 'Sai', 'saisudarson@titan.co.in', '8220715489', 'Titan', 'Bangalore ', '2021/03/05 12:35:02', '2021/03/05 13:15:47', '2021/03/05 12:35:02', 0),
(72, 'AVIK DAS', 'dasavik324@gmail.com', '7406463821', 'times of indi', 'BENGALURU', '2021/03/05 12:35:59', '2021/03/05 13:16:30', '2021/03/05 12:35:59', 0),
(73, 'bobby', 'bobby.tandon@adfactorspr.com', '9677067180', 'Adfactors', 'Chennai', '2021/03/05 12:36:52', '2021/03/05 13:15:23', '2021/03/05 12:36:52', 0),
(74, 'Siva kumar', 'dgm.digibank@sbi.co.in', '9619292945', 'Sbi', 'Mumbai', '2021/03/05 12:37:26', '2021/03/05 12:38:56', '2021/03/05 12:37:26', 0),
(75, 'Shrikant', 'himprabha@gmail.com', '8098768157', 'Himprabha', 'Chandigarh', '2021/03/05 12:37:33', '2021/03/05 17:52:35', '2021/03/05 12:37:33', 0),
(76, 'Priyanka dutta', 'priyanka86dutta@gmail.com', '9836690388', 'The Kolkata Mail', 'Kolkata', '2021/03/05 12:37:33', '2021/03/05 12:41:40', '2021/03/05 12:37:33', 0),
(77, 'Sangeetha B N', 'sangeethabn@titan.co.in', '9535312255', 'Titan', 'Corporate', '2021/03/05 12:38:01', '2021/03/05 13:12:27', '2021/03/05 12:38:01', 0),
(78, 'Saranya', 'saranyal@titan.co.in', '9731973006', 'Fastrack ', 'Bangalore ', '2021/03/05 12:41:08', '2021/03/05 13:15:54', '2021/03/05 12:41:08', 0),
(79, 'Sanjay', 'sanjaykurl@gmail.com', '9814825151', 'Punjab kesari', 'Chandigarh', '2021/03/05 12:51:00', '2021/03/05 12:56:45', '2021/03/05 12:44:23', 1),
(80, 'Sulabh Puri', 'sulabh@yamedianetworks.com', '9911100000', 'GadgetBridge.com', 'NEW DELHI', '2021/03/05 12:44:39', '2021/03/05 13:15:09', '2021/03/05 12:44:39', 0),
(81, 'Amar Shakti Prasad', 'ashakti.news@gmail.com', '9830646177', 'Prabhat Khabar ', 'Kolkata ', '2021/03/05 12:44:43', '2021/03/05 13:07:16', '2021/03/05 12:44:43', 0),
(82, 'Veena ', 'veena.kumari@gmail.com', 'n/a', 'Kannada Prabha', 'Bangalore ', '2021/03/05 12:58:06', '2021/03/05 13:02:14', '2021/03/05 12:44:53', 1),
(83, 'rahul khatry', 'rajksa@gmail.com', '7986985440', 'Sakht Khabar', 'Chandigarh', '2021/03/05 12:45:00', '2021/03/05 12:46:31', '2021/03/05 12:45:00', 0),
(84, 'Kavalam Sasikumar', 'kavalamsasikumar@gmail.com', '09446530279', 'Janmabhumi', 'Kochi', '2021/03/05 12:46:36', '2021/03/05 13:02:07', '2021/03/05 12:46:36', 0),
(85, 'Lakshmi Rajan', 'contactdesk@gmail.com', '8123237628', 'Tech Tamizha', 'Bangalore', '2021/03/05 12:48:22', '2021/03/05 13:28:34', '2021/03/05 12:48:22', 0),
(86, 'CP Singh', 'cpsingh@gmail.com', '9821654321', 'NewsZNew', 'Chandigarh ', '2021/03/05 12:50:11', '2021/03/05 13:16:42', '2021/03/05 12:50:11', 0),
(87, 'Sujatha ', 'sujatja@coact.co.in', '9845563760', 'Business world ', 'Bangalore ', '2021/03/05 12:52:05', '2021/03/05 12:52:36', '2021/03/05 12:52:05', 0),
(88, 'Vijaya Lakshmi', 'vijayalakshmi.m@manipalmedia.com', 'n/a', 'Udayavani', 'Bengaluru', '2021/03/05 12:52:26', '2021/03/05 13:11:02', '2021/03/05 12:52:26', 0),
(89, 'JEEVANKUMAR A', 'jeevan.journo@gmail.com', '07012195951', 'Malayala manorama', 'Ernakulam', '2021/03/05 13:02:32', '2021/03/05 13:06:08', '2021/03/05 13:02:32', 0),
(90, 'V', 'viraj@coact.co.in', '9765875731', 'Cc', 'Pp', '2021/03/05 13:04:18', '2021/03/05 13:25:30', '2021/03/05 13:04:18', 0),
(91, 'Aaron Dias', 'aaron.dias@adfactorspr.com', '9967987404', 'Adf', 'Mum', '2021/03/05 13:08:02', '2021/03/05 13:21:32', '2021/03/05 13:08:02', 0),
(92, 'Akshat', 'akshat@coaxt.co.in', '7204420017', 'Coact', 'Bng', '2021/03/05 13:08:11', '2021/03/05 13:18:12', '2021/03/05 13:08:11', 0),
(93, 'Lalit Indoria', 'l.k.indoria@gmail.com', '776065957', 'Hackmyandroid', 'Bangalore', '2021/03/05 14:41:29', '2021/03/05 14:42:29', '2021/03/05 14:41:29', 0),
(94, 'teeee', 'qweqe@asedad.com', '231312313123', 'wewqeqe', '21313', '2021/05/27 17:21:47', '2021/05/27 17:23:49', '2021/05/27 17:21:47', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
